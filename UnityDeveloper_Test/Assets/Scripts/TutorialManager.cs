using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialManager : MonoBehaviour
{
    [Tooltip("Array of PopUp gameobjects to be shown in order")]
    public GameObject[] PopUps;

    [Tooltip("Time for which the instructions are shown")]
    public float WaitTime = 5f;

    [Tooltip("Game object containg game instructions")]
    public GameObject Instruction;

    [Header("Other Script references")]
    [Tooltip("Reference to the Timer script")]
    public Timer Timer;

    [Tooltip("Reference to the Gravity Controller script")]
    public GravityController GravityController;

    // Index denoting which PopUp should be visible currently
    private int popUpIndex = 0;

    // Update is called once per frame
    void Update()
    {
        // Iterate over the PopUps and only activate the one corresponding the current popUpIndex
        for (int i = 0; i < PopUps.Length; i++)
        {
            PopUps[i].SetActive(i == popUpIndex);
        }

        
        if (popUpIndex == 0) // When the first PopUp is active [Movement Tutorial]
        {
            // If any movement key is pressed, move onto the next PopUp
            if (Input.GetKeyDown(KeyCode.W) ||
                Input.GetKeyDown(KeyCode.A) ||
                Input.GetKeyDown(KeyCode.S) ||
                Input.GetKeyDown(KeyCode.D) ||
                Input.GetKeyDown(KeyCode.LeftShift) ||
                Input.GetKeyDown(KeyCode.Space))
            {
                popUpIndex++;
            }
            // If the player activates hologram during the first tutorial only, skip to the third tutorial directly
            else if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.RightArrow))
            {
                popUpIndex = 2;
            }
        } 
        else if (popUpIndex == 1) // When the second PopUp is active [Hologram Activation Tutorial]
        {
            // If the hologram is activated, move onto the next PopUp
            if (GravityController.HoloVisible)
            {
                popUpIndex++;
            }
        } 
        else if (popUpIndex == 2) // When the third PopUp is active [Direction Selection Tutorial]
        {
            // If any Arrow Key is pressed (direction is selected), move onto the next PopUp
            if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.RightArrow))
            {
                popUpIndex++;
            }
            // If the player cancells the hologram, or sets the gravity to the default direction only, return back to the second tutorial
            else if (Input.GetKeyDown(KeyCode.Backspace) || Input.GetKeyDown(KeyCode.Return))
            {
                popUpIndex = 1;
            }
        }
        else if (popUpIndex == 3) // When the forth PopUp is active [Gravity Change Tutorial]
        {
            // If the Return Key is pressed, the tutorial is completed. Move onto the next PopUp
            if (Input.GetKeyDown(KeyCode.Return))
            {
                popUpIndex++;
            }
            // If the player cancells the hologram, move back to the second tutorial
            else if (Input.GetKeyDown(KeyCode.Backspace))
            {
                popUpIndex = 1;
            }
        }
        else if (popUpIndex == 4) // [Tutorial Complete]
        {
            // Display the instructions and start the timer
            Instruction.SetActive(true);
            Timer.StartTimer();

            // After the wait time is passed, remove the instructions
            if(WaitTime <= 0)
            {
                Instruction.SetActive(false);
            }
            else
            {
                WaitTime -= Time.deltaTime;
            }
        }
    }
}
